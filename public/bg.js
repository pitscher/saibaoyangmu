let d = 3; //divides Canvas = Windowsize by d

let bg = new p5((s) => {
    let theShader;
    s.preload = () => {
        // load the shader
        theShader = s.loadShader("shader/shader.vert", "shader/bg.frag");
    }

    s.setup = () => {
        // shaders require WEBGL mode to work
        let canvas = s.createCanvas(s.windowWidth / d, s.windowHeight / d, s.WEBGL);
        canvas.id('bg')
        canvas.attribute('style', 'position: absolute; left:0; top:0; z-index: -9;');
        // canvas.attribute('style', 'image-rendering: crisp-edges;image-rendering: pixelated;');
        canvas.style("width", "101%");
        canvas.style("height", "100%");
        canvas.parent("cloudcontainer");
        
        s.noStroke();
    }

    s.draw = () => {
        // shader() sets the active shader with our shader
        s.shader(theShader);
        // set uniforms for frag Shader to be used
        theShader.setUniform("iResolution", [s.width, s.height]);
        theShader.setUniform("iTime", s.frameCount * 0.01);
        // rect gives us some geometry on the screen
        s.rect(0, 0, s.width, s.height);
        // print out the framerate
        // console.log(frameRate())
    }
})